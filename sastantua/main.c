/* *************************************************************************** */
/*                                                                             */
/*                                                         :::      ::::::::   */
/*   sastantua.c                                         :+:      :+:    :+:   */
/*                                                     +:+ +:+         +:+     */
/*   by: MaRwEiN <contact@marwein.fr>                +#+  +:+       +#+        */
/*                                                 +#+#+#+#+#+   +#+           */
/*   Created: 2013/07/21 14:43:19 by MaRwEiN           #+#    #+#              */
/*   Updated: 2013/07/21 23:06:46 by MaRwEiN          ###   ########.fr        */
/*                                                                             */
/* *************************************************************************** */

#include "sastantua.h"

int  main(void)
{
	int	size;

	printf("specify the size of th pyramide to show : ");
	scanf("%d", &size);
	ft_putchar('\n');
	sastantua(size);
	return (0);
}
