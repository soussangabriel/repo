/* *************************************************************************** */
/*                                                                             */
/*                                                         :::      ::::::::   */
/*   sastantua.c                                         :+:      :+:    :+:   */
/*                                                     +:+ +:+         +:+     */
/*   by: MaRwEiN <contact@marwein.fr>                +#+  +:+       +#+        */
/*                                                 +#+#+#+#+#+   +#+           */
/*   Created: 2013/07/21 14:43:19 by MaRwEiN           #+#    #+#              */
/*   Updated: 2013/07/21 23:06:46 by MaRwEiN          ###   ########.fr        */
/*                                                                             */
/* *************************************************************************** */

#include "sastantua.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_height(int size, int full)
{
	int		visible_size;
	int		invisible_size;
	int		invisible_stage;

	visible_size = (size * (size + 5)) / 2;
	invisible_stage = (size + full - 1) / 2;
	invisible_size = invisible_stage * (invisible_stage + 3);
	if (size % 2 == full)
		invisible_size += invisible_stage + 2;

	return (visible_size + invisible_size);
}

char	ft_door_check(int x, int y, int height, int size)
{
	int		door_size;
	int		door_height;
	int		door_x;
	int		knob_x;
	int		knob_y;
	char	result;

	door_size = (size - 1) / 2;
	door_height = (door_size * 2) + 1;
	door_x = height;
	knob_x = door_x + door_size - 1;
	knob_y = height - door_size;

	if (y <= (height - door_height))
		result = '*';
	else if (x < (door_x - door_size) || x > (door_x + door_size))
		result = '*';
	else if (door_size >= 2 && x == knob_x && y == knob_y)
		result = '$';
	else
		result = '|';

	return (result);
}

void	ft_build_row(int width, int height, int size)
{
	int		padding;
	int		position;

	padding = height - width;
	position = 0;
	while (position < (padding + (2*width)))
	{
		if (position < padding)
			ft_putchar(' ');
		else if (position == padding)
			ft_putchar('/');
		else if (position > padding)
			ft_putchar(ft_door_check(position, width, height, size));

		position++;
	}

	ft_putchar('\\');
	ft_putchar('\n');
}

void	sastantua(int size)
{
	int		height = ft_height(size, 0);
	int		i;
	int		stage;
	int		visible_stage_size;
	int		invisible_stage_size;

	stage = 1;
	i = 0;
	while (stage <= size)
	{
		visible_stage_size = ft_height(stage, 0);
		invisible_stage_size = ft_height(stage, 1);

		while (i < invisible_stage_size)
		{
			if (i < visible_stage_size)
				ft_build_row(i+1, height, size);
			i++;
		}
		stage++;
	}
}
