/* *************************************************************************** */
/*                                                                             */
/*                                                         :::      ::::::::   */
/*   sastantua.c                                         :+:      :+:    :+:   */
/*                                                     +:+ +:+         +:+     */
/*   by: MaRwEiN <contact@marwein.fr>                +#+  +:+       +#+        */
/*                                                 +#+#+#+#+#+   +#+           */
/*   Created: 2013/07/21 14:43:19 by MaRwEiN           #+#    #+#              */
/*   Updated: 2013/07/21 23:06:46 by MaRwEiN          ###   ########.fr        */
/*                                                                             */
/* *************************************************************************** */

#ifndef SASTANTUA_H
# define SASTANTUA_H
# include <unistd.h>
# include <stdio.h>

void	ft_putchar(char c);
int		ft_height(int size, int full);
char	ft_door_check(int x, int y, int height, int size);
void	ft_build_row(int width, int height, int size);
void	sastantua(int size);

#endif /* !SASTANTUA_H */