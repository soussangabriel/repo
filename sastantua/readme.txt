Sastantua---------
Url     : http://codes-sources.commentcamarche.net/source/100027-sastantuaAuteur  : MaRwEiNDate    : 29/07/2013
Licence :
=========

Ce document intitul� � Sastantua � issu de CommentCaMarche
(codes-sources.commentcamarche.net) est mis � disposition sous les termes de
la licence Creative Commons. Vous pouvez copier, modifier des copies de cette
source, dans les conditions fix�es par la licence, tant que cette note
appara�t clairement.

Description :
=============

  
<br />
<br /><pre>
/* ****************************************************
*********************** */    
/*                                              
                               */    
/*                                       
                  :::      ::::::::   */    
/*   sastantua.c                  
                       :+:      :+:    :+:   */    
/*                         
                            +:+ +:+         +:+     */    
/*   by: MaRwEiN &lt
;contact@marwein.fr&gt;                +#+  +:+       +#+        */    
/*     
                                            +#+#+#+#+#+   +#+           */    

/*   Created: 2013/07/21 14:43:19 by MaRwEiN           #+#    #+#              *
/    
/*   Updated: 2013/07/21 23:06:46 by MaRwEiN          ###   ########.fr  
      */    
/*                                                                
             */    
/* ********************************************************
******************* */    

#include &lt;unistd.h&gt;    
#include &lt;stdio.
h&gt;    

void    ft_putchar(char c)    
{    
 write(1, &amp;c, 1);    
}
    

int  ft_height(int size, int full)    
{    
 int  visible_size;    

 int  invisible_size;    
 int  invisible_stage;    

 visible_size = (size *
 (size + 5)) / 2;    
 invisible_stage = (size + full - 1) / 2;    
 invisible
_size = invisible_stage * (invisible_stage + 3);    
 if (size % 2 == full)    

  invisible_size += invisible_stage + 2;    

 return (visible_size + invisi
ble_size);    
}    

char ft_door_check(int x, int y, int height, int size) 
   
{    
 int  door_size;    
 int  door_height;    
 int  door_x;    
 in
t  knob_x;    
 int  knob_y;    
 char result;    

 door_size = (size - 1) 
/ 2;    
 door_height = (door_size * 2) + 1;    
 door_x = height;    
 knob_
x = door_x + door_size - 1;    
 knob_y = height - door_size;    

 if (y &lt
;= (height - door_height))    
  result = '*';    
 else if (x &lt; (door_x - 
door_size) || x &gt; (door_x + door_size))    
  result = '*';    
 else if (d
oor_size &gt;= 2 &amp;&amp; x == knob_x &amp;&amp; y == knob_y)    
  result = 
'$';    
 else    
  result = '|';    

 return (result);    
}    

void
 ft_build_row(int width, int height, int size)    
{    
 int  padding;    
 
int  position;    

 padding = height - width;    
 position = 0;    
 while
 (position &lt; (padding + (2*width)))    
 {    
  if (position &lt; padding)
    
   ft_putchar(' ');    
  else if (position == padding)    
   ft_putcha
r('/');    
  else if (position &gt; padding)    
   ft_putchar(ft_door_check(
position, width, height, size));    

  position++;    
 }    

 ft_putchar
('\\');    
 ft_putchar('\n');    
}    

void sastantua(int size)    
{   
 
 int  height = ft_height(size, 0);    
 int  i;    
 int  stage;    
 int 
 visible_stage_size;    
 int  invisible_stage_size;    

 stage = 1;    
 i
 = 0;    
 while (stage &lt;= size)    
 {    
  visible_stage_size = ft_heig
ht(stage, 0);    
  invisible_stage_size = ft_height(stage, 1);    

  while 
(i &lt; invisible_stage_size)    
  {    
   if (i &lt; visible_stage_size)   
 
    ft_build_row(i+1, height, size);    
   i++;    
  }    
  stage++;   
 
 }    
}    

int  main(void)    
{    
 int size;    

 printf(&quot;
specify the size of th pyramide to show : &quot;);    
 scanf(&quot;%d&quot;, &
amp;size);    
 ft_putchar('\n');    
 sastantua(size);    
 return (0);    

}   

</pre>
